//WAP to find the sum of two fractions.
#include <stdio.h>

typedef struct
{

int num;
int den;
}frac;

frac input()

{
    frac z;
    printf("Kindly enter the numerator : \n");
    scanf("%d",&z.num);
    printf("Kindly enter the denominator : \n");
    scanf("%d",&z.den);

    return z;
}
int gcd(int m,int n)
{
    int x=1,i;
    for(i=2;i<=m&&i<=n;++i)
    {
        if(m%i==0&&n%i==0)
        x=i;
    }
    return x;
}
frac compute(frac a,frac b)
{
    frac k;
    int num,den;
    num=(a.num*b.den+a.den*b.num);
    den=a.den*b.den;
    k.num=num/gcd(num,den);
    k.den=den/gcd(num,den);
    return k;
}
void result(frac a,frac b,frac k)
{
    printf("Sum %d/%d and %d/%d is  = %d/%d \n",a.num,a.den,b.num,b.den,k.num,k.den);
}
int main()
{
    frac a,b,k;
    a=input();
    b=input();
    k=compute(a,b);
    result(a,b,k);
    return 0;
}



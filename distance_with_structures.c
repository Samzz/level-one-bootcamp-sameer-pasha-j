//WAP to find the distance between two points using structures and 4 functions.

# include<stdio.h>
#include<math.h>
struct point
{
 float p;
float q;
};
int input01()
{
float p;
printf("enter x-coordinate :\n");
scanf("%f",&p);
return p;
}
int input02()
{
float q;
printf("enter y-coordinate :\n");
scanf("%f",&q);
return q;
}
float distance(float p1,float p2,float q1, float q2)
{
float dist;
dist= sqrt((p2-p1)*(p2-p1)+(q2-q1)*(q2-q1));
return dist;
}
void display(float p1, float p2, float q1, float q2, float dist)
{
printf("Distance between %f,%f and %f,%f is =%f",p1,p2,q1,q2,dist);
}
int main()
{
float p1,p2,q1,q2,dist;
printf("enter p1 :\n");
p1=input01();
printf("enter q1 :\n");
q1=input02();
printf("enter p2 :\n");
p2=input01();
printf("enter q2 :\n");
q2=input02();
dist=distance(p1,p2,q1,q2);
return dist;
}
